//Dikshant Adhikari
//Lab Section 22
#ifndef LINKEDLIST_CPP_
#define LINKEDLIST_CPP_

#include "Node.cpp"

template<class T> class LinkedList{
public:
    Node<T>* headPointer = nullptr;

    //insert item to the location of the head pointer and set the head pointer to the new item
    void insert(T item){
        headPointer = new Node<T>(item, headPointer);
    }

    //remove the item head point is pointing to and set the next pointer to the next item in the list
    T* remove() {
        if(headPointer == nullptr){
            return nullptr;
        }
        Node<T>* p = headPointer;
        headPointer = headPointer -> nextHead;
        return &p-> listData;
    }

    //access the item the head pointer is pointing to
    T* accessHead(){
        if(headPointer == nullptr){
            return nullptr;
        }

        return &headPointer-> listData;
    }
};


#endif