//Dikshant Adhikari
//Lab Section 22

#ifndef NODE_CPP_
#define NODE_CPP_

template<class T> class Node {

public:
    T listData;
    Node* nextHead;

public:
    Node(T& item, Node* nextNode = nullptr){
        listData = item;
        nextHead = nextNode;
    }
};

#endif
