//Dikshant Adhikari
//Lab Section 22

#include "Node.cpp"
#include "LinkedList.cpp"
#include "StackADT.h"
#include "Underflow.h"

template<class T> class LinkedListStack:public StackADT<T>{
public:
    LinkedList<T> linkedList;

    ~LinkedListStack(){
    }

    //check if list is empty
    bool Empty(){
        return linkedList.accessHead() == nullptr;
    }

    //push item into the list
    void Push(T& item){
        linkedList.insert(item);
    }

    //Pop the last item from stack
    T Pop(){
        T* element = linkedList.remove();
        if(element == nullptr){
            throw Underflow();
        }
        return *element;
    }

    //returns the last item from the stack
    T Top(){
        T* element = linkedList.accessHead();
        if (element == nullptr){
            throw Underflow();
        }
        return *element;
    }


};
