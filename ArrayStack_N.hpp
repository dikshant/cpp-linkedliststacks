//Dikshant Adhikari
//Lab Section 22

#ifndef _ARRAYSTACK_N_HPP_
#define _ARRAYSTACK_N_HPP_

#include "StackADT.h"
#define DEFAULT_SIZE 50

template<class T>
class ArrayStack_N : public StackADT<T> {

private:

    T *arrayStack; //A pointer to point towards the location of the arrayStack
    int index; //a variable to store the number of items in an array and keep track of its index
    int maxSize; //the maximum size of the arrayStack

public:

    // a constructor for the arrayStack that creates and arrayStack of a given size
    ArrayStack_N(int size = DEFAULT_SIZE) {
        maxSize = size;
        index = 0;
        arrayStack = new T[size];
    }

    // a destructor that deletes the arrayStack
    ~ArrayStack_N() {
    }

    //checks if the array is empty
    bool Empty() {
        return index==0;
    }


    //checks to see if there is enough room in array and then only pushes item into the array
    //if there is not enough room then it increases the size of the array by duplicating it and inserting new item
    void Push(T& item = 0) {
        if (index<maxSize) {
            arrayStack[index]= item;
            index+=1;
        }else {
            T* tempArrayStack = arrayStack; //making a copy of the array stack
            arrayStack = new T[2*maxSize];
            maxSize = 2*maxSize;

            for (int i=0; i<index; i++){
                arrayStack[i]=tempArrayStack[i];
            }
            arrayStack[index]=item;
            index+=1;
            delete[] tempArrayStack;
        }
    }

    T Top() {
        if(index == 0){
            return -1;
        }else{
            T element = arrayStack[index--];
            return element;
        }
    }

    T Pop() {
        if(index == 0){
            return -1;
        }else {
            T element = arrayStack[index];
            index--;
            return element;
        }
    }
};


#endif