//Dikshant Adhikari
// Lab Section 22

#ifndef _ARRAYSTACK_1_HPP_
#define _ARRAYSTACK_1_HPP_

#include "StackADT.h"
#define DEFAULT_SIZE 50

template<class T>
class ArrayStack_1 : public StackADT<T> {

private:

    T *arrayStack; //A pointer to point towards the location of the array stack
    int index; //a variable to store the number of items in an array stack and keep track of its index
    int maxSize; //the maximum size of the array stack

public:

    // a constructor for the arrayStack that creates and array stack of a given size
    ArrayStack_1(int size = DEFAULT_SIZE) {
        maxSize = size;
        index = 0;
        arrayStack = new T[size];
    }

    //a destructor that deletes the arrayStack
    ~ArrayStack_1() {
    }

    //checks if the array is empty
    bool Empty() {
        return index==0;
    }

    //checks to see if there is enough room in array and then only pushes item into the array
    //if there is not enough room then it increases the size of the array by duplicating it and inserting new item
    void Push(T& item = 0) {
        if (index<maxSize) {
            arrayStack[index]= item;
            index+=1;
        }else {
            T* tempArrayStack = arrayStack; //making a copy of the array stack
            arrayStack = new T[++maxSize];
            for (int i=0; i<index; i++){
                arrayStack[i]=tempArrayStack[i];
            }

            delete[] tempArrayStack;
            arrayStack[index]=item;
            index+=1;

        }
    }

    //remove the top item
    T Pop() {
            if(index==0){
                return -1;
            }else {
                T element = arrayStack[index];
                index--;
                return element;
            }
    }

    //returns the last item in the stack from the top
    T Top() {
        if(index != 0){
            T element = arrayStack[index-1];
            return element;
        }else{
            return -1;
        }
    }
};


#endif